import { IsNotEmpty, MinLength, IsPositive } from 'class-validator';
export class CreateProductDto {
  id: number;
  @IsNotEmpty()
  @MinLength(8)
  name: string;
  @IsPositive()
  @IsNotEmpty()
  price: number;
}
