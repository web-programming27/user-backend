import { Injectable } from '@nestjs/common';
import { NotFoundException } from '@nestjs/common/exceptions';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let products: Product[] = [
  { id: 1, name: 'PEURRi Acne Cleanser', price: 500 },
  { id: 2, name: 'Physiogel Cleanser', price: 350 },
  { id: 3, name: 'Acne-Aid liquid Cleanser', price: 430 },
];
let lastProductsId = 4;
@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastProductsId++,
      ...createProductDto, //name,price
    };
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    //console.log('product' + JSON.stringify(products[index]));
    //console.log('update' + JSON.stringify(updateProductDto));
    const updateProduct: Product = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deleteProduct = products[index];
    products.splice(index, 1);
    return deleteProduct;
  }
  reset() {
    products = [
      { id: 1, name: 'PEURRi Acne Cleanser', price: 500 },
      { id: 2, name: 'Physiogel Cleanser', price: 350 },
      { id: 3, name: 'Acne-Aid liquid Cleanser', price: 430 },
    ];
    lastProductsId = 4;
    return 'RESET';
  }
}
